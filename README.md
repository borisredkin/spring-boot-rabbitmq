# README #

Demo application demonstrating Spring Boot connection to a RabbitMQ.

* Run RabbitMQ

```
#!
$ docker run -d -p 5672:5672 -p 15672:15672 rabbitmq:management
```
* Run Spring Boot app

```
#!
$ spring-boot:run
```
* Verify the connection queue

http://localhost:15672/#/queues